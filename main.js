let bird;
let bars = [];
let score;

function startGame() {
    bird = new component(30, 30, 'yellow', 300, 120);
    bird.gravity = 0.05;
    score = new component('30px', 'Consolas', 'black', 400, 40, 'text');
    gameArea.start();
}

let gameArea = {
    canvas: document.createElement('canvas'),
    start: function () {
        this.canvas.width = 800;
        this.canvas.height = 600;
        this.context = this.canvas.getContext('2d');
        document.body.insertBefore(this.canvas, document.body.childNodes[0]);
        this.frameNo = 0;
        this.interval = setInterval(updateGameArea, 20);
    },
    clear: function () {
        this.context.clearRect(0, 0, this.canvas.width, this.canvas.height);
    }
}

function component(width, height, color, x, y, type) {
    this.type = type;
    this.score = 0;
    this.width = width;
    this.height = height;
    this.speedX = 0;
    this.speedY = 0;
    this.x = x;
    this.y = y;
    this.gravity = 0;
    this.gravitySpeed = 0;
    this.update = function () {
        ctx = gameArea.context;
        if (this.type == 'text') {
            ctx.font = this.width + ' ' + this.height;
            ctx.fillStyle = color;
            ctx.fillText(this.text, this.x, this.y);
        } else {
            ctx.fillStyle = color;
            ctx.fillRect(this.x, this.y, this.width, this.height);
        }
    }
    this.newPos = function () {
        this.gravitySpeed += this.gravity;
        this.x += this.speedX;
        this.y += this.speedY + this.gravitySpeed;
        this.hitBottom();
    }
    this.hitBottom = function () {
        let rockbottom = gameArea.canvas.height - this.height - 30;
        if (this.y > rockbottom) {
            this.y = rockbottom;
            this.gravitySpeed = 0;
        }
    }
    this.crashWith = function (otherobj) {
        let myleft = this.x;
        let myright = this.x + (this.width);
        let mytop = this.y;
        let mybottom = this.y + (this.height);
        let otherleft = otherobj.x;
        let otherright = otherobj.x + (otherobj.width);
        let othertop = otherobj.y;
        let otherbottom = otherobj.y + (otherobj.height);
        let crash = true;
        if ((mybottom < othertop) || (mytop > otherbottom) || (myright < otherleft) || (myleft > otherright)) {
            crash = false;
        }
        return crash;
    }
}

function updateGameArea() {
    let x, height, gap, minHeight, maxHeight, minGap, maxGap;
    for (i = 0; i < bars.length; i += 1) {
        if (bird.crashWith(bars[i])) {
            return;
        }
    }
    gameArea.clear();
    gameArea.frameNo += 1;
    if (gameArea.frameNo == 1 || everyinterval(175)) {
        x = gameArea.canvas.width;
        minHeight = 10;
        maxHeight = 400;
        height = Math.floor(Math.random() * (maxHeight - minHeight + 1) + minHeight);
        minGap = 80;
        maxGap = 175;
        gap = Math.floor(Math.random() * (maxGap - minGap + 1) + minGap);
        bars.push(new component(40, height, 'green', x, 0));
        bars.push(new component(40, x - height - gap, 'green', x, height + gap));
    }
    for (i = 0; i < bars.length; i += 1) {
        bars[i].x += -1;
        bars[i].update();
    }
    score.text = parseInt(+ gameArea.frameNo / 100);
    score.update();
    bird.newPos();
    bird.update();
}

function everyinterval(n) {
    if ((gameArea.frameNo / n) % 1 == 0) { return true; }
    return false;
}

window.addEventListener('keydown', (event) => {
    if (event.key === ' ') {
        accelerate(-0.8);
        return;
    }
    else if (keyName === 'Enter') {
        location.reload();
        return;
    }
});

window.addEventListener('keyup', (event) => {
    if (event.key === ' ') {
        accelerate(0.18);
        return;
    }
});

function accelerate(n) {
    bird.gravity = n;
}